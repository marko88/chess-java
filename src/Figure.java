
import javax.swing.ImageIcon;

/**
 * Apstaktna klasa definise figure, i njihove zajednicke metode.
 * 
 * Svaka klasa potomak ove klase, tipa specificne figure, mora da implementira 
 * metodu setTartgetsAndMovementFields() koja definise specifican nacin kretanja 
 * figure
 * 
 * Svaka realna klasa mora da promeni isBlack() ili isWhite() zavisno od boje
 * 
 * @author marko
 */
abstract class Figure {

    protected ImageIcon blackIcon;
    protected ImageIcon whiteIcon;
    protected ImageIcon icon;
    
    public Figure() {}

    protected final ImageIcon getBlackIcon() {
        return blackIcon;
    }

    protected final ImageIcon getWhiteIcon() {
        return whiteIcon;
    }

    public final ImageIcon getIcon() {
        return icon;
    }
    
    public boolean isBlack() {
        return false;
    }
    
    public boolean isWhite() {
        return false;
    }
    
    /**
     * Oznacava sva polja po kojima figura moze da se krece
     */
    abstract public void setTartgetsAndMovementFields();
    
    /**
     * Postavlja i proverava(dali je slobodno) polje kretanja figure koje se krecu linearno
     * 
     * @param nextX
     * @param nextY
     * @return 
     */
    protected boolean targetNextFieldForLinearFigure(int nextX, int nextY) {
        Field nextField = Table.getField(nextX, nextY);
        Table.checkAndSetTargetOn(nextField);
        return !nextField.hasFigure();
    }
    
    /**
     * Daje kopiju prosedjenog tipa
     * 
     * @param figure Figura za kopiju
     * @return Figure Objekat nove figure tipa figure
     */
    public static Figure getCopyOf(Figure figure) {
        if (figure instanceof WhiteBishop)
            return new WhiteBishop();
        else if (figure instanceof WhiteKing)
            return new WhiteKing();
        else if (figure instanceof WhiteKnight)
            return new WhiteKnight();
        else if (figure instanceof WhitePawn)
            return new WhitePawn();
        else if (figure instanceof WhiteQueen)
            return new WhiteQueen();
        else if (figure instanceof WhiteRook)
            return new WhiteRook();
        else if (figure instanceof BlackBishop)
            return new BlackBishop();
        else if (figure instanceof BlackKing)
            return new BlackKing();
        else if (figure instanceof BlackKnight)
            return new BlackKnight();
        else if (figure instanceof BlackPawn)
            return new BlackPawn();
        else if (figure instanceof BlackQueen)
            return new BlackQueen();
        else if (figure instanceof BlackRook)
            return new BlackRook();
        else
            return null;
    }
}