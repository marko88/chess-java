
/**
 * Klasa sa definijom Kolja
 * 
 * @author marko
 */
abstract class Knight extends Figure {

    public Knight() {
        this.blackIcon = Utility.getIcon(Config.BLACK_KNIGHT_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_KNIGHT_ICON, Config.ICON_SIZE);
    }
    
    @Override
    public void setTartgetsAndMovementFields() {
        Field selectedField = Table.getSelectedField();
        int x = selectedField.getPOSITION_X();
        int y = selectedField.getPOSITION_Y();
        
        if (x > 1 && y > 2) {
            Field upLeft = Table.getField(x-1, y-2);
            Table.checkAndSetTargetOn(upLeft);
        }
        
        if (x < 8 && y > 2) {
            Field upRight = Table.getField(x+1, y-2);
            Table.checkAndSetTargetOn(upRight);
        }
        
        if (x < 7 && y > 1) {
            Field rightUp = Table.getField(x+2, y-1);
            Table.checkAndSetTargetOn(rightUp);
        }
        
        if (x < 7 && y < 8) {
            Field rightDown = Table.getField(x+2, y+1);
            Table.checkAndSetTargetOn(rightDown);
        }
        
        if (x < 7 && y < 7) {
            Field downRight = Table.getField(x+1, y+2);
            Table.checkAndSetTargetOn(downRight);
        }
        
        if (x > 1 && y < 7) {
            Field downLeft = Table.getField(x-1, y+2);
            Table.checkAndSetTargetOn(downLeft);
        }
        
        if (x > 2 && y < 8) {
            Field leftDown = Table.getField(x-2, y+1);
            Table.checkAndSetTargetOn(leftDown);
        }
        
        if (x > 2 && y > 1) {
            Field leftUp = Table.getField(x-2, y-1);
            Table.checkAndSetTargetOn(leftUp);
        }
    }
}

/**
 * Realna klasa sa belim konjem
 * 
 * @author marko
 */
class WhiteKnight extends Knight {

    public WhiteKnight() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnim konjem
 * 
 * @author marko
 */
class BlackKnight extends Knight {

    public BlackKnight() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}

