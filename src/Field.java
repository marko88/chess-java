
import java.awt.Color;
import javax.swing.JButton;

/**
 * Klasa za definicju polja, svako polje moze da sadrzi u sebi jednu figuru ili
 * moze biti prazno, u sebi cuva kordinate da bi figura(ako je sarzi mogla da 
 * odredi polja po kojima koze da se krece
 * 
 * Svako polje dobija ikonu i gubi je po postavljanu ili ulanjanju figure
 * 
 * @author marko
 */
class Field extends JButton {
    
    private final int POSITION_X;
    private final int POSITION_Y;
    
    private Figure figure = null;
    
    private boolean isTarget = false;

    /**
     * Postavlja kordinate i boju polja
     * 
     * @param x
     * @param y 
     */
    public Field(int x, int y) {
        this.POSITION_X = x;
        this.POSITION_Y = y;
        
        setDefaultBackgroundColor();
    }
    
    public Field() {
        this(0,0);
    }
    
    /**
     * Postavlja poju polja, crnu ili belu
     */
    public final void setDefaultBackgroundColor() {
        int x = POSITION_X;
        int y = POSITION_Y;
        
        if (y%2 == 0 && x%2 == 0)
            setBackground(Config.WHITE_FIELD_COLOR);
        else if (y%2 == 0 && x%2 != 0)
            setBackground(Config.BLACK_FIELD_COLOR);
        else if (y%2 != 0 && x%2 == 0)
            setBackground(Config.BLACK_FIELD_COLOR);
        else
            setBackground(Config.WHITE_FIELD_COLOR);
    }
    
    /**
     * Menja boju polja u selektovanu
     */
    public final void setSelectedBackgroundColor() {
        setBackground(Config.SELECTED_FIELD_COLOR);
    }
    
    /**
     * Oznacava polje kao metu i menja mu boju
     */
    public void setTargetOn() {
        this.isTarget = true;
        if (hasFigure())
            setBackground(Config.TARGET_FIELD_COLOR);
        else
            setBackground(Config.MOVEMENT_FIELD_COLOR);
    }

    /**
     * Postavlja figuru i postavlja ikonu polja(ikonu figure)
     * 
     * @param figure 
     */
    public void setFigure(Figure figure) {
        this.figure = figure;
        
        setIcon(figure.getIcon()); 
    }
    /**
     * Uklanja figuru sa polja i brise ikonu polja
     */
    public void removeFigure() {
        this.figure = null;
        setIcon(null);
    }
    
    public boolean isTarget() {
        return this.isTarget;
    }

    public int getPOSITION_X() {
        return POSITION_X;
    }

    public int getPOSITION_Y() {
        return POSITION_Y;
    }

    public Figure getFigure() {
        return figure;
    }
    
    public boolean hasFigure() {
        return figure != null;
    }
    
    public void setTargetOff() {
        this.isTarget = false;
        setDefaultBackgroundColor();
    }
    
    public void markFieldWithRed() {
        setBackground(Color.RED);
    }
    
    
}
