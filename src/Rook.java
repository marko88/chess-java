
/**
 * Klasa sa definijom Topa
 * 
 * @author marko
 */
abstract class Rook extends Figure {

    public Rook() {
        this.blackIcon = Utility.getIcon(Config.BLACK_ROOK_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_ROOK_ICON, Config.ICON_SIZE);
    }
    
    @Override
    public void setTartgetsAndMovementFields() {
        Field selectedField = Table.getSelectedField();
        int x = selectedField.getPOSITION_X();
        int y = selectedField.getPOSITION_Y();
        int nextX, nextY;
        
        // Up Direcion
        nextX = x;
        nextY = y - 1;
        while (nextY >= 1) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextY--;
        }
        
        // Down Direcion
        nextX = x;
        nextY = y + 1;
        while (nextY <= 8) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextY++;
        }
        
        // Left Direcion
        nextX = x - 1;
        nextY = y;
        while (nextX >= 1) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX--;
        }
        
        // Right Direcion
        nextX = x + 1;
        nextY = y;
        while (nextX <= 8) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX++;
        }
    }
}

/**
 * Realna klasa sa belim topom
 * 
 * @author marko
 */
class WhiteRook extends Rook {

    public WhiteRook() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnim topom
 * 
 * @author marko
 */
class BlackRook extends Rook {

    public BlackRook() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}
