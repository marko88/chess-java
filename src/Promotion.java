
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;

/**
 * Prozor za zamenu pijuna sa jednom od 4 figura Kraljicom, Topom, Lovcem ili Koljem
 * Skriva prozor sa tabelom dok se ne izabere figura
 * 
 * @author marko
 */
public class Promotion extends JFrame{
    
    /**
     * Konstruktor pravi 4 polja sa figurama za zamenu, postavlja ActionListener
     * za svako od polja koji menja pijuna za figuru na polju sa ActionListener-om
     * 
     * @param field
     */
    public Promotion(Field field) {
        // prvo sakri tablu
        Main.getFrame().setVisible(false);
        
        Figure pown = field.getFigure();
        
        setFrameSettings();
        
        Figure[] blackExchangeFigures = {
            new BlackQueen(), 
            new BlackRook(), 
            new BlackBishop(), 
            new BlackKnight()
        };
        
        Figure[] whiteExchangeFigures = {
            new WhiteQueen(), 
            new WhiteRook(), 
            new WhiteBishop(), 
            new WhiteKnight()
        };
        
        // Pravi tabelu od 4 elementa
        for (int i = 0; i < 4; i++) {
            Field newField = new Field(); // pravi novo polje
            Figure newFigure = (pown.isBlack()) ? blackExchangeFigures[i] : whiteExchangeFigures[i];
            newField.setFigure(newFigure);
            
            add(newField); // ubacuje polje u tablu

            // podesava ActionEvent za polje, kada igrac klikne na polje ono ce
            // zameniti pijuna za novu figuru koja se nalazi na ovom polju
            newField.addActionListener((ActionEvent e) -> {
                // postavi novu figuru na mestu pijuna
                field.setFigure(newFigure);
                
                // zatvori prozor i vrati tablu
                Main.getFrame().setVisible(true);
                this.dispose();
            });
        }
    }
    
    private void setFrameSettings() {
        setTitle(Config.PROMOTION_WINDOW_TITLE);
        setSize(Config.PROMOTION_WINDOW_WIDTH, Config.PROMOTION_MAIN_WINDOW_HEIGHT);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setResizable(false);
        setLayout(new GridLayout(1, 4));
    }
    
}
