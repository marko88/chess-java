
/**
 * Klasa sa definijom Kralja
 * 
 * @author marko
 */
abstract class King extends Figure {

    public King() {
        this.blackIcon = Utility.getIcon(Config.BLACK_KING_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_KING_ICON, Config.ICON_SIZE);
    }
    
    @Override
    public void setTartgetsAndMovementFields() {
        Field selectedField = Table.getSelectedField();
        int x = selectedField.getPOSITION_X();
        int y = selectedField.getPOSITION_Y();
        
        // Up-Left
        if (x > 1 && y > 1) {
            Field upLeft = Table.getField(x-1, y-1);
            Table.checkAndSetTargetOn(upLeft);
        }
        
        // Up
        if (y > 1) {
            Field up = Table.getField(x, y-1);
            Table.checkAndSetTargetOn(up);
        }
        
        // Up-Right
        if (x < 8 && y > 1) {
            Field upRight = Table.getField(x+1, y-1);
            Table.checkAndSetTargetOn(upRight);
        }
        
        // Right
        if (x < 8) {
            Field right = Table.getField(x+1, y);
            Table.checkAndSetTargetOn(right);
        }
        
        // Down-Right
        if (x < 8 && y < 8) {
            Field downRight = Table.getField(x+1, y+1);
            Table.checkAndSetTargetOn(downRight);
        }
        
        // Down
        if (y < 8) {
            Field down = Table.getField(x, y+1);
            Table.checkAndSetTargetOn(down);
        }
        
        // Down-Left
        if (x > 1 && y < 8) {
            Field downLeft = Table.getField(x-1, y+1);
            Table.checkAndSetTargetOn(downLeft);
        }
        
        // Left
        if (x > 1) {
            Field left = Table.getField(x-1, y);
            Table.checkAndSetTargetOn(left);
        }
    }
}

/**
 * Realna klasa sa belim kraljem
 * 
 * @author marko
 */
class WhiteKing extends King {

    public WhiteKing() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnim kraljem
 * 
 * @author marko
 */
class BlackKing extends King {

    public BlackKing() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}