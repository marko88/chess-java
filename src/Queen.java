
/**
 * Klasa sa definijom Kraljice
 * 
 * @author marko
 */
abstract class Queen extends Figure {

    public Queen() {
        this.blackIcon = Utility.getIcon(Config.BLACK_QUEEN_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_QUEEN_ICON, Config.ICON_SIZE);
    }
    
    @Override
    public void setTartgetsAndMovementFields() {
        Rook rook = new WhiteRook();
        rook.setTartgetsAndMovementFields();
        Bishop bishop = new WhiteBishop();
        bishop.setTartgetsAndMovementFields();
    }
}

/**
 * Realna klasa sa belom kraljicom
 * 
 * @author marko
 */
class WhiteQueen extends Queen {

    public WhiteQueen() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnom kraljicom
 * 
 * @author marko
 */
class BlackQueen extends Queen {

    public BlackQueen() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}
