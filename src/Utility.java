
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 * Sadrzi porisne alate za podesavalje igre
 * 
 * @author marko
 */
public class Utility {
    
    private Utility() {}
    
    /**
     * Metoda za promenu dimenizja date slike
     * 
     * @param srcImg Objekat sa skliom tipa Image
     * @param w Nova sirina slike
     * @param h Nova visina slike
     * @return Image Objekat sa predimenzionisanom slikom
     */
    public static final Image getScaledImage(Image srcImg, int w, int h){
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();

        return resizedImg;
    }
    
    /**
     * Metod za generisanje ikone
     * 
     * @param location Lokacija slije u fajl sistemu
     * @param size Zeljena velicina slike
     * @return ImageIcon Objekat sa ikonom
     */
    public static final ImageIcon getIcon(String location, int size) {
        // napravi url resors, da bi ikonice bile priakzane u jar arhivi
        URL resource = ClassLoader.getSystemResource(location);
        ImageIcon newIcon = new ImageIcon(resource);
        Image image = newIcon.getImage();
        Image resizedIcon = getScaledImage(image, size, size);
        newIcon.setImage(resizedIcon);
        
        return newIcon;
    }
    
}
