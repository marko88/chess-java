
import javax.swing.*;

/**
 * Osnovna klasa aplikacije, pravi prozor u JFrame-u, podesava ga i pravi tabelu
 * @author Marko
 */
public class Main extends JFrame{
    
    public static JFrame frame;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Main sah = new Main();
        Main.frame = sah;
        
        sah.setTitle(Config.MAIN_WINDOW_TITLE);
        sah.setSize(Config.MAIN_WINDOW_WIDTH, Config.MAIN_WINDOW_HEIGHT);
        sah.setLocationRelativeTo(null);
        sah.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        sah.setVisible(true);
        sah.setResizable(false);
    }
    
    public Main() {
        Table table = new Table();
        add(table);
    }
    
    public static JFrame getFrame() {
        return Main.frame;
    }
    
}

