
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * Tablela sa poljima za igru, u sebi sadrzi pravila igre
 * 
 * @author marko
 */
class Table extends JPanel {
    
    private static final Field[][] table = new Field[9][9];
    
    private boolean whiteOnTheMove;
    private boolean blackOnTheMove;
    
    private boolean isCheck = false;
    private boolean isMate = false;
    private boolean isBlackCheck = false;
    private boolean isWhiteCheck = false;
    private Figure checkedKingFigure;
    private Field checkedKingField;
    
    private Field blackKingField;
    private Field whiteKingField;
    
    private static Field selectedField = null;
    
    /**
     * Pravi tablu 8x8 i od svakog elementa tabele pravi polje tipa Field
     * postavlja pocene figure i za svako polje postavlja ActionEvent
     */
    public Table() {
        setLayout(new GridLayout(8, 8));
        
        // Pravi tabelu
        for (int y = 1; y <= 8; y++) {
            for (int x = 1; x <= 8; x++) {
                Field newField = new Field(x, y); // pravi novo polje
                setStartingPositionForFigure(newField, x, y); // stavlja pocetnu figuru
                Table.table[x][y] = newField; // ubacuje polje u niz
                
                add(newField); // ubacuje polje u tablu
                
                // podesava ActionEvent za polje
                newField.addActionListener((ActionEvent e) -> {
                    onClick(newField);
                });
            }
        }
        
        whiteIsOnTheMove();
    }
    
    /**
     * Postavlja pocetnu figuru za dato polje
     * 
     * @param field Polje u kome se postavlja figura
     * @param x Horizntalna koordinata polja
     * @param y Vertikalna koodrinata polja
     */
    private void setStartingPositionForFigure(Field field, int x, int y) {
        if (isStartingPositionForBlackPown(x, y))
            field.setFigure(new BlackPawn());
        
        if (isStartingPositionForBlackRook(x, y))
            field.setFigure(new BlackRook());
        
        if (isStartingPositionForBlackKnight(x, y))
            field.setFigure(new BlackKnight());
        
        if (isStartingPositionForBlackBishop(x, y))
            field.setFigure(new BlackBishop());
        
        if (isStartingPositionForBlackQueen(x, y))
            field.setFigure(new BlackQueen());
        
        if (isStartingPositionForBlackKing(x, y))
            field.setFigure(new BlackKing());
        
        if (isStartingPositionForWhitePown(x, y))
            field.setFigure(new WhitePawn());
        
        if (isStartingPositionForWhiteRook(x, y))
            field.setFigure(new WhiteRook());
        
        if (isStartingPositionForWhiteKnight(x, y))
            field.setFigure(new WhiteKnight());
        
        if (isStartingPositionForWhiteBishop(x, y))
            field.setFigure(new WhiteBishop());
        
        if (isStartingPositionForWhiteQueen(x, y))
            field.setFigure(new WhiteQueen());
        
        if (isStartingPositionForWhiteKing(x, y))
            field.setFigure(new WhiteKing());
    }
    
    /**
     * ActionEvent svakog polja, sobzirom da aplikacija reaguje iskljucivo na klik
     * ovo je glavna metoda aplikacije jer kontrolise igru putem klika na nekom od polja
     * 
     * @param currentField 
     */
    private void onClick(Field currentField) {
        // ako je mat izbaci poruku pa izadji
        if (isMate()) {
            JOptionPane.showMessageDialog(this, "Mat");
            return;
        }
        
        // proverava dali igrac moze da upravlja selektovanom figurom
        if (!playerOnTheMove(currentField))
            return;
        
        // ako je sah oznaci kralja crvenom bojom
        if (isCheck())
            getCheckedKingField().markFieldWithRed();
            
        // ako je izabrana figura u prethodnom potezu i selektovano novo polje
        if (hasSelectedField() && currentField.isTarget()) {
            // ako moze da izvede potez, prepusti potez drugom igracu
            if (tryToMove(currentField)) {
                nextPlayerOnTheMove();
                checkForCheck();
                if (isCheck()) { // ako je sah proveri mat
                    if (checkForMate())
                        JOptionPane.showMessageDialog(this, "Mat");
                    resetFields();
                    return;
                }
            }
            
        }

        // ako figura nije izabrana i selektovano polje sarzi figuru
        if (!hasSelectedField() && currentField.hasFigure()) {
            setSelectedField(currentField);
            setTartgetsForSelectedField();
        }
        else resetFields(); // u suprotnom samo resetuj podesavanja meta i polja
        
    }
    
    /**
     * Pokusava da izvede pokret pod uslovima, da se time ne pravi sah sopstvenom timu,
     * ili ako je pijun dosao do zadnjeg polja izbacuje prozor za zamenu figura
     * 
     * @param currentField Zeleljena destinacija za prethodno selektovanu figuru
     * @return boolean
     */
    private boolean tryToMove(Field currentField) {
        // moze da napadne kralja, jer je znacen kao meta zbog vidljivosti saha
        if (areAllies(selectedField.getFigure(), currentField.getFigure()))
            return false;
        
        // selectedField se menja zbog checkForCheck(), mislim da vrti selektovane fajlove
        // zato mora da se koristi moveFieldToField() metoda
        Field selectedFieldCopy = selectedField;
        
        // kopija neprijateljske figure
        Figure figureCopy = null;
        if (currentField.getFigure() != null) {
            if (isEnemy(currentField.getFigure())) {
                figureCopy = Figure.getCopyOf(currentField.getFigure());
            }
        }

        // odigraj potez
        moveFieldToField(selectedField, currentField);

        // Proverava sah posle odigranog poteze
        // Ako postoji sah posle odigranog poteza, vraca prethodno odigran potez
        // Sah moze postojati i pre odigranog poteza, ako potez nije ukolio sah vracaju se figure
        checkForCheck();
        if (isCheck() && isCheckOnTheMySide(currentField)) {
            
            // vrati prethodni potez
            moveFieldToField(currentField, selectedFieldCopy);
            
            // pomeranjem fidura se poremeti polozaj checkedKingField polja
            // checkForCheck() ponovo proverava polozaje kraljeva
            checkForCheck();
            
            // ako je u trethodnom potezu uzeta neprijateljska figura, vrati je
            if (figureCopy != null)
                currentField.setFigure(figureCopy);
            
            return false;
        }
        else {
            // proverava dali je pijun dosao do polja za zamenu figura, tj. zadnjeg polja
            if (currentField.getFigure() instanceof Pawn) {
                if (isWhitePromotionForField(currentField) || isBlackPromotionForField(currentField)) {
                    // izbaci prozor za zamenu figura
                    new Promotion(currentField);
                }
            }
            
            return true;
        }
    }
    
    /**
     * Proverava mat, tako sto selektuje svako polje na tabli, ako sadrzi saveznicku
     * figuru(figuru Kralja pod sahom pomera je na svim dozvoljenim poljima i 
     * ako nijedan pokret ne uklanja sah onda je mat
     * 
     * @return boolean
     */
    private boolean checkForMate() {
        boolean isMate = true;
        Figure kingUnderCheck = getCheckedKingFigure();
        
        // Uzima sva polja na tabli, svako zasebno
        for (int x_c = 1; x_c <= 8; x_c++) {
            for (int y_c = 1; y_c <= 8; y_c++) {
                Field currentField = getField(x_c, y_c);
                
                // Proverava dali polje sarzi figuru i dali je taj tim na potezu
                if (currentField.hasFigure()) {
                    Figure currentFigure = currentField.getFigure();
                    setSelectedField(currentField); // Postavlja selektovanu figuru
                    if (isAlly(kingUnderCheck)) {

                        currentFigure.setTartgetsAndMovementFields(); // postavlja mete

                        // Ponovo okrece polja da pronadje polja po kojima moze figura da se krece
                        for (int x = 1; x <= 8; x++) {
                            for (int y = 1; y <= 8; y++) {
                                Field targetField = getField(x, y);
                                
                                // resetuje podesavalja, posto ih tryFictiveMoveToDisableCheck menja
                                resetFields();
                                setSelectedField(currentField);
                                currentFigure.setTartgetsAndMovementFields();

                                if (targetField.isTarget())
                                    // ako postoji pokret koji ce ponistiti sah uklanja pocetni mat
                                    if (tryFictiveMoveToDisableCheck(targetField)) {
                                        resetFields();
                                        isMate = false;
                                    }
                            }
                        }
                    }
                }
            }
        }
        
        resetFields();
        
        this.isMate = isMate;
        
        return isMate;
    }
    
    /**
     * Pokusava da pomeri selektovanu figuru na polje currentField, i proverava
     * dali to ulanja sah
     * 
     * @param currentField Polje za pomeraj selektovane figure
     * @return boolean
     */
    private boolean tryFictiveMoveToDisableCheck(Field currentField) {
        // moze da napadne kralja, jer je znacen kao meta zbog vidljivosti saha
        if (areAllies(selectedField.getFigure(), currentField.getFigure()))
            return false;
        
        // selectedField se menja zbog checkForCheck(), mislim da vrti selektovane fajlove
        // zato mora da se koristi moveFieldToField() metoda
        Field selectedFieldCopy = selectedField;
        
        // kopija neprijateljske figure
        Figure figureCopy = null;
        if (currentField.getFigure() != null) {
            if (isEnemy(currentField.getFigure())) {
                figureCopy = Figure.getCopyOf(currentField.getFigure());
            }
        }

        // odigraj potez
        moveFieldToField(selectedField, currentField);

        // Proverava sah posle odigranog poteze
        // Ako postoji sah posle odigranog poteza, vraca prethodno odigran potez
        // Sah moze postojati i pre odigranog poteza, ako potez nije ukolio sah vracaju se figure
        checkForCheck();
        
        boolean isCheckAfterMove = (isCheck());
        
        // vrati prethodni potez
        moveFieldToField(currentField, selectedFieldCopy);

        // pomeranjem fidura se poremeti polozaj checkedKingField polja
        // checkForCheck() ponovo proverava polozaje kraljeva
        checkForCheck();

        // ako je u trethodnom potezu uzeta neprijateljska figura, vrati je
        if (figureCopy != null)
            currentField.setFigure(figureCopy);
            
        return !isCheckAfterMove;
    }
    
    /**
     * Proverava dali je sah, tako sto za svaku figuru na tabli oznacava polja
     * po kojima ona moze da se krece, i ako kraljeva polja nisu oznacena onda 
     * nije ni sah
     */
    private void checkForCheck() {
        resetFields();
        
        setAllTargetedFieldsForTheNextMove();
        setKingsFields();
        
        // Proveri polje crnog kralja, ako je meta u narednom potezu stavi sah
        if (blackKingField.hasFigure())
            setCheckOnTheKingIfTaget(blackKingField);
        
        // Proveri polje belog kralja, ako je meta u narednom potezu stavi sah
        if (whiteKingField.hasFigure())
            setCheckOnTheKingIfTaget(whiteKingField);
        
        // Ako oba kraljeva polja nisu mete, ponisti sah sa obe strane
        if (blackKingField.hasFigure() && whiteKingField.hasFigure()) {
            if (!blackKingField.isTarget() && !whiteKingField.isTarget()) {
                setCheckOff();
            }
        }
    }
    
    /**
     * Proverava dali je dato kraljevo polje kingsField oznaceno kao meta
     * 
     * @param kingsField Kraljevo polje za proveru
     */
    private void setCheckOnTheKingIfTaget(Field kingsField) {
        Figure king = kingsField.getFigure();
        if (kingsField.isTarget()) {
            this.isCheck = true;
            this.isWhiteCheck = (king.isWhite());
            this.isBlackCheck = (king.isBlack());
            kingsField.setTargetOn();
            setCheckOn(king, kingsField);
        }
    }
    
    /**
     * Obelezava sva polja koja svaka od figura moze da napadne u narednom potezu
     * setTartgetsAndMovementFields() pravi razliku prijateljskih i neprijateljskih figura
     * 
     */
    private void setAllTargetedFieldsForTheNextMove() {
        for (int x = 1; x <= 8; x++) {
            for (int y = 1; y <= 8; y++) {
                Field currentField = getField(x, y);
                if (currentField.hasFigure()) {
                    Figure currentFigure = currentField.getFigure();
                    setSelectedField(currentField);
                    
                    currentFigure.setTartgetsAndMovementFields();
                }
            }
        }
    }
    
    /**
     * Postavlja kraljeva polja blackKingField i whiteKingField za trenutni rapored
     * 
     */
    private void setKingsFields() {
        for (int x = 1; x <= 8; x++) {
            for (int y = 1; y <= 8; y++) {
                Field currentField = getField(x, y);
                if (currentField.hasFigure()) {
                    Figure currentFigure = currentField.getFigure();

                    if (currentFigure instanceof BlackKing) {
                        blackKingField = currentField;
                    }

                    if (currentFigure instanceof WhiteKing) {
                        whiteKingField = currentField;
                    }
                }
            }
        }
    }
    
    /**
     * Proverava dali je sah na strani igraca koji je na potezu
     * 
     * @param field Selektovano polje sa figurom na potezu
     * @return 
     */
    private boolean isCheckOnTheMySide(Field field) {
        Figure figure = field.getFigure();
        if (figure.isBlack() && this.isWhiteCheck)
            return false;
        else if (figure.isWhite() && this.isBlackCheck)
            return false;
        else
            return true;
    }
    
    /**
     * Postavlja mete(polja po kojima moze da se krece) za selektovanu figuru
     */
    private void setTartgetsForSelectedField() {
        Figure selectedFigure = getSelectedField().getFigure();
        selectedFigure.setTartgetsAndMovementFields();
        
        if (!hasTargets())
            resetFields();
    }
    
    /**
     * Postavlja sah
     * 
     * @param kingFigure Kraljeva figura pod sahom
     * @param kingField Kraljevo polje pod sahom
     */
    private void setCheckOn(Figure kingFigure, Field kingField) {
        this.isCheck = true;
        this.checkedKingFigure = kingFigure;
        this.checkedKingField = kingField;
    }
    
    /**
     * Ponistava sah
     */
    private void setCheckOff() {
        this.isCheck = false;
        this.isBlackCheck = false;
        this.isWhiteCheck = false;
        this.checkedKingFigure = null;
        this.checkedKingField = null;
    }

    /**
     * Postavlja selektovano polje
     * 
     * @param selectedField Kliknuto polje sa figurom na potezu
     */
    public void setSelectedField(Field selectedField) {
        Table.selectedField = selectedField;
        Table.selectedField.setSelectedBackgroundColor();
    }
    
    /**
     * Ponistava selektovano polje
     */
    private void removeSelectedField() {
        Table.selectedField = null;
    }
    
    /**
     * Proverava dali je selektovano polje za pomeraj figure
     * 
     * @return boolean
     */
    public boolean hasSelectedField() {
        return (getSelectedField() instanceof Field);
    }
    
    /**
     * Razmenjuje figure u datim poljima, iz field1 u field2
     * 
     * @param field1 Polje sa figurom
     * @param field2 Polje za novo mesto figure
     */
    private void moveFieldToField(Field field1, Field field2) {
        if (!areAllies(field1.getFigure(), field2.getFigure()) &&
                field1 != field2) {
            field2.setFigure(field1.getFigure());
            field1.removeFigure();
        }
    }
    
    /**
     * Postavlja drugi tim na potezu
     */
    private void nextPlayerOnTheMove() {
        if (isWhiteOnTheMove())
            blackIsOnTheMove();
        else
            whiteIsOnTheMove();
    }
    
    /**
     * Ponistava mete na svim poljima
     */
    private void resetFields() {
        for (Field[] fields : table) {
            for (Field currentField : fields) {
                if (currentField instanceof Field) {
                    currentField.setTargetOff();
                    currentField.setDefaultBackgroundColor();
                }
            }
            
        }
        removeSelectedField();
    }
    
    /**
     * Proverava dali postoji makar jedno polje oznaceno kao meta
     * 
     * @return boolean
     */
    private boolean hasTargets() {
        for (Field[] fields : table) {
            for (Field currentField : fields) {
                if (currentField instanceof Field) {
                    currentField.isTarget();
                    return true;
                }
            }
            
        }
        return false;
    }
    
    /**
     * Proverava dali je kliknuta figura, figura na polju currentField, na potezu
     * 
     * @param currentField Kliknuto polje
     * @return boolean
     */
    private boolean playerOnTheMove(Field currentField) {
        if (currentField.hasFigure()) {
            if (hasSelectedField())
                return true;
            else if (currentField.getFigure().isBlack() && isBlackOnTheMove())
                return true;
            else return (currentField.getFigure().isWhite() && isWhiteOnTheMove());
        }
        else return true;
    }
    
    /**
     * Oznacava polje kao metu(menja isTarget polje), ako je prazno ili ima neprijateljsku figuru
     * 
     * @param field 
     */
    public static void checkAndSetTargetOn(Field field) {
        Figure selectedFigure = selectedField.getFigure();
        
        if (field.hasFigure()) {
            if (areEnemies(field.getFigure(), selectedFigure)) {
                field.setTargetOn();
            }
        }
        else
            field.setTargetOn();
    }
    
    public static boolean areEnemies(Figure figure1, Figure figure2) {
        return !((figure1.isWhite() && figure2.isWhite()) || (figure1.isBlack() && figure2.isBlack()));
    }
    
    public static boolean areAllies(Figure figure1, Figure figure2) {
        if (figure1 == null || figure2 == null) {
            return false;
        }
        return ((figure1.isWhite() && figure2.isWhite()) || (figure1.isBlack() && figure2.isBlack()));
    }
    
    public static boolean isEnemy(Figure figure) {
        Figure selectedFigure = getSelectedField().getFigure();
        return !((selectedFigure.isWhite() && figure.isWhite()) || (selectedFigure.isBlack() && figure.isBlack()));
    }
    
    public static boolean isAlly(Figure figure) {
        return !(isEnemy(figure));
    }

    public static Field getField(int x, int y) {
        return Table.table[x][y];
    }
    
    private void whiteIsOnTheMove() {
        this.whiteOnTheMove = true;
        this.blackOnTheMove = false;
    }
    
    private void blackIsOnTheMove() {
        this.whiteOnTheMove = false;
        this.blackOnTheMove = true;
    }
    
    public boolean isWhiteOnTheMove() {
        return this.whiteOnTheMove;
    }
    
    public boolean isBlackOnTheMove() {
        return this.blackOnTheMove;
    }
    
    public boolean isCheck() {
        return this.isCheck;
    }
    
    public boolean isMate() {
        return this.isMate;
    }
    
    private Figure getCheckedKingFigure() {
        return this.checkedKingFigure;
    }
    
    private Field getCheckedKingField() {
        return this.checkedKingField;
    }

    public static Field getSelectedField() {
        return selectedField;
    }
    
    private boolean isWhitePromotionForField(Field field) {
        return field.getFigure().isWhite() && field.getPOSITION_Y() == 1;
    }
    
    private boolean isBlackPromotionForField(Field field) {
        return field.getFigure().isBlack() && field.getPOSITION_Y() == 8;
    }
    
    private boolean isStartingPositionForWhitePown(int x, int y) {
        return y == 7;
    }
    private boolean isStartingPositionForWhiteRook(int x, int y) {
        return y == 8 && ( x == 1 || x == 8 );
    }
    private boolean isStartingPositionForWhiteKnight(int x, int y) {
        return y == 8 && ( x == 2 || x == 7 );
    }
    private boolean isStartingPositionForWhiteBishop(int x, int y) {
        return y == 8 && ( x == 3 || x == 6 );
    }
    private boolean isStartingPositionForWhiteQueen(int x, int y) {
        return y == 8 && x == 4;
    }
    private boolean isStartingPositionForWhiteKing(int x, int y) {
        return y == 8 && x == 5;
    }
    
    private boolean isStartingPositionForBlackPown(int x, int y) {
        return y == 2;
    }
    private boolean isStartingPositionForBlackRook(int x, int y) {
        return y == 1 && ( x == 1 || x == 8 );
    }
    private boolean isStartingPositionForBlackKnight(int x, int y) {
        return y == 1 && ( x == 2 || x == 7 );
    }
    private boolean isStartingPositionForBlackBishop(int x, int y) {
        return y == 1 && ( x == 3 || x == 6 );
    }
    private boolean isStartingPositionForBlackQueen(int x, int y) {
        return y == 1 && x == 4;
    }
    private boolean isStartingPositionForBlackKing(int x, int y) {
        return y == 1 && x == 5;
    }
}
