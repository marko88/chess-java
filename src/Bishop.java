
/**
 * Klasa sa definijom Lovca
 * 
 * @author marko
 */
abstract class Bishop extends Figure {

    public Bishop() {
        this.blackIcon = Utility.getIcon(Config.BLACK_BISHOP_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_BISHOP_ICON, Config.ICON_SIZE);
    }
    
    @Override
    public void setTartgetsAndMovementFields() {
        Field selectedField = Table.getSelectedField();
        int x = selectedField.getPOSITION_X();
        int y = selectedField.getPOSITION_Y();
        int nextX, nextY;
        
        // Up Left Direcion
        nextX = x - 1;
        nextY = y - 1;
        while (nextX >= 1 && nextY >= 1) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX--;
            nextY--;
        }
        
        // Up Right Direction
        nextX = x + 1;
        nextY = y - 1;
        while (nextX <= 8 && nextY >= 1) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX++;
            nextY--;
        }
        
        // Down Right Direction
        nextX = x + 1;
        nextY = y + 1;
        while (nextX <= 8 && nextY <= 8) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX++;
            nextY++;
        }
        
        // Down Left Direction
        nextX = x - 1;
        nextY = y + 1;
        while (nextX >= 1 && nextY <= 8) {
            if (!targetNextFieldForLinearFigure(nextX, nextY))
                break;
            nextX--;
            nextY++;
        }
    }
}

/**
 * Realna klasa sa belim lovcem
 * 
 * @author marko
 */
class WhiteBishop extends Bishop {

    public WhiteBishop() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnim lovcem
 * 
 * @author marko
 */
class BlackBishop extends Bishop {

    public BlackBishop() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}
