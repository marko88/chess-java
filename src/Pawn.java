
/**
 * Klasa sa definijom Pesaka
 * 
 * @author marko
 */
abstract class Pawn extends Figure {

    public Pawn() {
        this.blackIcon = Utility.getIcon(Config.BLACK_POWN_ICON, Config.ICON_SIZE);
        this.whiteIcon = Utility.getIcon(Config.WHITE_POWN_ICON, Config.ICON_SIZE);
    }
    
    
    @Override
    public void setTartgetsAndMovementFields() {
        Field selectedField = Table.getSelectedField();
        int x = selectedField.getPOSITION_X();
        int y = selectedField.getPOSITION_Y();
        
        
        boolean conditionForWhite = (selectedField.getFigure().isWhite() && y >= 2);
        boolean conditionForBlack = (selectedField.getFigure().isBlack() && y <= 7);
        
        if (conditionForWhite || conditionForBlack) {
            Field nextField = (conditionForWhite) ? Table.getField(x, y-1) : Table.getField(x, y+1);
            if (!nextField.hasFigure())
                nextField.setTargetOn();
            
            if (x >= 2 && y >= 2) {
                Field nextLeft = (conditionForWhite) ? Table.getField(x-1, y-1) : Table.getField(x-1, y+1);
                if (x > 1 && nextLeft.hasFigure() && Table.isEnemy(nextLeft.getFigure()))
                    nextLeft.setTargetOn();
            }
            
            if (x <= 7 && y >= 2) {
                Field nextRight = (conditionForWhite) ? Table.getField(x+1, y-1) : Table.getField(x+1, y+1);
                if (x < 8 && nextRight.hasFigure() && Table.isEnemy(nextRight.getFigure()))
                    nextRight.setTargetOn();
            }
            
            // Fist duble move, from start position
            boolean isWhiteOnStartPosition = (y == 7 && !Table.getField(x, 6).hasFigure() && isWhite());
            boolean isBlackOnStartPosition = (y == 2 && !Table.getField(x, 3).hasFigure() && isBlack());
            
            if (isWhiteOnStartPosition || isBlackOnStartPosition) {
                nextField = (isWhiteOnStartPosition) ? Table.getField(x, y-2) : Table.getField(x, y+2);
                if (!nextField.hasFigure())
                    nextField.setTargetOn();
            }
        }
    }
}

/**
 * Realna klasa sa belim pesakom
 * 
 * @author marko
 */
class WhitePawn extends Pawn {

    public WhitePawn() {
        this.icon = getWhiteIcon();
    }
    
    @Override
    public boolean isWhite() {
        return true;
    }
}

/**
 * Realna klasa sa crnim pesakom
 * 
 * @author marko
 */
class BlackPawn extends Pawn {

    public BlackPawn() {
        this.icon = getBlackIcon();
    }
    
    @Override
    public boolean isBlack() {
        return true;
    }
}