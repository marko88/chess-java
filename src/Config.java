
import java.awt.Color;

/**
 * Klasa sa konfiguracionim konstantama, za centralizovano podesavanje igre
 * 
 * @author marko
 */
public class Config {
    
    private Config() {}
    
    public static final String MAIN_WINDOW_TITLE = "Chess";
    public static final int MAIN_WINDOW_WIDTH = 500;
    public static final int MAIN_WINDOW_HEIGHT = 500;
    
    public static final String PROMOTION_WINDOW_TITLE = "Promotion";
    public static final int PROMOTION_WINDOW_WIDTH = 250;
    public static final int PROMOTION_MAIN_WINDOW_HEIGHT = 90;
    
    public static final int ICON_SIZE = 60;
    
    public static final Color WHITE_FIELD_COLOR = Color.WHITE;
    public static final Color BLACK_FIELD_COLOR = Color.LIGHT_GRAY;
    
    public static final Color SELECTED_FIELD_COLOR = Color.GREEN;
    public static final Color TARGET_FIELD_COLOR = Color.RED;
    public static final Color MOVEMENT_FIELD_COLOR = Color.YELLOW;
    
    public static final String ICON_LOCATION = "images/";
    public static final String BLACK_POWN_ICON   = ICON_LOCATION + "pawnb.gif";
    public static final String WHITE_POWN_ICON   = ICON_LOCATION + "pawnw.gif";
    public static final String BLACK_KING_ICON   = ICON_LOCATION + "kingb.gif";
    public static final String WHITE_KING_ICON   = ICON_LOCATION + "kingw.gif";
    public static final String BLACK_QUEEN_ICON  = ICON_LOCATION + "queenb.gif";
    public static final String WHITE_QUEEN_ICON  = ICON_LOCATION + "queenw.gif";
    public static final String BLACK_ROOK_ICON   = ICON_LOCATION + "rookb.gif";
    public static final String WHITE_ROOK_ICON   = ICON_LOCATION + "rookw.gif";
    public static final String BLACK_BISHOP_ICON = ICON_LOCATION + "bishopb.gif";
    public static final String WHITE_BISHOP_ICON = ICON_LOCATION + "bishopw.gif";
    public static final String BLACK_KNIGHT_ICON = ICON_LOCATION + "knightb.gif";
    public static final String WHITE_KNIGHT_ICON = ICON_LOCATION + "knightw.gif";
    
    
}
